package common

import java.io.IOException

/**
 * Describes '@cd', '@ls' and '@pwd' commands for [server.Server] and some necessary
 * methods to handle output of processes which will be executed while using this class.
 * @param path  the current path.
 *
 * @author Alex Chernenkov
 */
class Commands {
    private var path: String? = null

    init{
        path = "/"
    }

    /**
     * '@cd' command : change directory.
     * @param command  the command, which's been received by [server.Server]
     * @return         current directory.
     */
    fun changeDirectory(command: String): String? {
        val cmd = command.split(" ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        if (cmd.size == 1 && cmd[0] == "@cd") {
            path = "/"
            return path
        }
        if (cmd[1] == "..") {
            if (path == "/") {
                path = "/"
            } else {
                path = path!!.substring(0, path!!.lastIndexOf('/'))
                if (path!!.isEmpty()) {
                    path = "/"
                }
            }
        } else {
            var tmpPath: String? = path
            tmpPath += if (path == "/") cmd[1] else "/" + cmd[1]
            if (checkPath(tmpPath) == 0) {
                path = tmpPath
            } else {
                return "Невозможно получить доступ к " + cmd[1] + ": нет такого файла или каталога"
            }
        }
        return path
    }

    /**
     * '@ls' command : list all files and directories in the current directory.
     * @return list of files and directories in current directory.
     */

    private fun checkPath(directory: String?): Int {
        var value = 0
        try {
            val process = Runtime.getRuntime().exec("ls " + directory)
            process.waitFor()
            value = process.exitValue()
        } catch (e: IOException) {
            e.printStackTrace()
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }
        return value
    }

    /**
     * '@pwd' command : shows absolute path to the current directory.
     * @return Absolute path to the current directory.
     */
    fun getPath(): String? {
        return path
    }
}