package server

import common.Commands
import user.User
import java.io.BufferedReader
import java.io.InputStreamReader
import java.net.DatagramPacket
import java.net.DatagramSocket
import java.net.InetAddress

/**
 * Class describes the server side of 2-user UDP socket chat.
 * @param user          keeps data class [User] instance with name set.
 * @param serverSocket  keeps instance of [DatagramSocket] for this server.
 * @param currentDir    keeps path to current dir in [String].
 * @param commands      keeps instance of [Commands].
 * @param sendData      keeps data, which needs to be sent in [ByteArray].
 * @param getData       keeps received data in [ByteArray], built from [receivePacket].
 * @param sendPacket    instance of [DatagramPacket], built from [sendData].
 * @param receivePacket instance of [DatagramPacket].
 * @param getMessage    text of received message, built from [getData].
 * @param sendMessage   text of message to be sent.
 * @param clientAddress instance of [InetAddress], keeps client's address.
 * @param clientPort    keeps port for client.
 * @author Alex Chernenkov
 */
class Server(private val port: Int) : Thread() {
    private var user: User = User("")
    private var serverSocket: DatagramSocket = DatagramSocket()
    private var currentDir = "/"
    private var commands: Commands = Commands()
    private var sendData: ByteArray = ByteArray(65507)
    private var getData: ByteArray = ByteArray(65507)
    private var sendPacket: DatagramPacket = DatagramPacket(sendData, sendData.size)
    private var receivePacket: DatagramPacket = DatagramPacket(getData, getData.size)
    private var getMessage: String? = ""
    private var sendMessage: String? = ""
    private var clientAddress: InetAddress = InetAddress.getByName("")
    private var clientPort: Int = receivePacket.port

    /**
     * The main method which makes server to receive and send
     */
    override fun run(){
        user.name = "anon_user"
        serverSocket = DatagramSocket(port)
        println("Connection established: ${serverSocket.isBound}")
        while (serverSocket.isBound && !serverSocket.isClosed && !quit()){
            sendMessage = ""
            receive()
            //
            send()
        }
    }

    /**
     * For server we need both to close socket and to disconnect it
     */
    private fun disconnect() {
        serverSocket.close()
        serverSocket.disconnect()
    }

    /**
     * Receive method. Describes the receive part of server, handles all commands: [quit],
     * [ls], [pwd], [cd] and @name: the procedure of client's name changing.
     */
    private fun receive(){
        if(!serverSocket.isClosed) {
            getMessage = ""
            receivePacket = DatagramPacket(getData, getData.size)
            serverSocket.receive(receivePacket)
            getMessage = String(receivePacket.data, 0, receivePacket.length)

            if (getMessage.equals("@quit")) disconnect()
            if (getMessage!!.contains("@name")) {
                val tmp = getMessage!!.split("@name ")
                user.name = tmp[1]
                println("Anon user changed name to, ${user.name} ")
            }
            if (getMessage!! == "@pwd") pwd()
            if (getMessage!! == "@ls") ls()
            if(getMessage!!.contains("@cd")) cd(getMessage!!)
            println("[${user.name}]: $getMessage")
        }
    }

    /**
     * Send method. Handles servers replies. After [receive] in [run], [sendMessage] is set
     * to "". If there were some commands requested in [receive], [sendMessage] will
     * be changed by one of commands [ls], [pwd], [cd]. If [sendMessage] wasn't changed, it
     * still have "" value and will be interpreted to as-non command message. So will be
     * read from input with [readLine].
     */
    private fun send(){
        if(!serverSocket.isClosed) {
            clientAddress = receivePacket.address
            clientPort = receivePacket.port
            if(sendMessage == "") sendMessage = readLine() // !!!
            sendData = sendMessage!!.toByteArray()
            sendPacket = DatagramPacket(sendData, sendData.size, clientAddress, clientPort)
            serverSocket.send(sendPacket)
            if (sendMessage.equals("@quit")) disconnect()
        }
    }

    /**
     * If server receives or sends @quit command, it returns true. Else false.
     */
    private fun quit() = getMessage.equals("@quit") || sendMessage.equals("@quit")

    /**
     * Sets [sendMessage] to keep the absolute path to the current directory.
     */
    private fun pwd() {
        sendMessage = commands.getPath()
    }

    /**
     * Calls changeDirectory() method from [Commands] and sets current path with getPath()
     * from [Commands]. Sets [sendMessage] to keep the path, to which moved at the moment.
     */
    private fun cd(cmd: String){
        sendMessage = commands.changeDirectory(cmd)
        currentDir = commands.getPath()!!
    }

    /**
     * Sets [sendMessage] to keep list of all files and directories in the current path.
     */
    private fun ls() {
        sendMessage = "\n"
        val read = BufferedReader(InputStreamReader(Runtime.getRuntime().exec("ls $currentDir").inputStream))
        while (true) {
            sendMessage += read.readLine() + "\n"
            if(read.readLine() == null) break
        }
    }
}