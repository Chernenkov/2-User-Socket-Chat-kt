package client

import java.net.DatagramPacket
import java.net.DatagramSocket
import java.net.InetAddress
// TODO: Try listener thread and sender thread
class Client(address: String, private val port: Int = 55000): Thread() {
    private var messageSend: String? = ""
    private var messageGet: String? = ""
    private val address: InetAddress = InetAddress.getByName(address)
    private var clientSocket: DatagramSocket = DatagramSocket()
    private var sendData: ByteArray = ByteArray(65507)
    private  var getData: ByteArray = ByteArray(65507)
    private var sendPacket: DatagramPacket = DatagramPacket(sendData, sendData.size)
    private var receivePacket: DatagramPacket = DatagramPacket(getData, getData.size)
    //
    override fun run(){
        //clientSocket = DatagramSocket(port, address)
        clientSocket.connect(address, port)
        println("Connection established: ${clientSocket.isConnected}")
        while(clientSocket.isConnected && !quit()){
            //send part
            send()
            //receive part
            receive()
        }
    }
    private fun disconnect() {
        clientSocket.disconnect()
        clientSocket.close()
    }

    private fun send(){
        if(!clientSocket.isClosed) {
            messageSend = readLine()
            sendData = messageSend!!.toByteArray()
            sendPacket = DatagramPacket(sendData, sendData.size, address, port)
            clientSocket.send(sendPacket)
            if (messageSend == "@quit") disconnect()
        }
    }

    private fun receive(){
        if(!clientSocket.isClosed) {
            receivePacket = DatagramPacket(getData, getData.size)
            clientSocket.receive(receivePacket)
            messageGet = String(receivePacket.data, 0, receivePacket.length)
            println("[SERVER]: $messageGet")
            if (messageGet.equals("@quit")) disconnect()
        }
    }

    private fun quit(): Boolean = messageGet.equals("@quit") || messageSend.equals("@quit")
}